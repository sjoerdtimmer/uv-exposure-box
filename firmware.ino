/*
Rotary Encoder - Polling Example
    
The circuit:
* encoder pin A to Arduino pin 2
* encoder pin B to Arduino pin 3
* encoder ground pin to ground (GND)
*/

#include <MD_REncoder.h>
#include <Bounce2.h>
#include "LedControl.h"

bool running = false;
MD_REncoder encoder = MD_REncoder(2, 3);
Bounce button = Bounce();
LedControl disp=LedControl(7,5,6,1); 

void setup() 
{
  Serial.begin(9600);
  encoder.begin();

   // Setup the button with an internal pull-up :
  pinMode(4, INPUT_PULLUP);
  button.attach(4);
  button.interval(5); // interval in ms

  disp.shutdown(0, false);
  disp.setIntensity(0,15);

  // setup output led:
  pinMode(8, OUTPUT);
}

int timer = 0; // 0 and -1 indicates the two phases of the buzzer
unsigned long prevmillis = 0;

void printTime()
{
  if (timer < 0)
  {
    disp.clearDisplay(0);
    return;
  }
  int mins = (timer) / 60;
  int secs = (timer) % 60;
  Serial.print("time: ");
  Serial.print(mins);
  Serial.print(secs < 10?":0":":");
  Serial.println(secs);

  if (mins/10>0)
  {
    disp.setDigit(0, 3, mins/10,0);
  }
  else
  {
    disp.setChar(0,3, ' ', false);
  }
  disp.setDigit(0, 2, mins%10,1);
  disp.setDigit(0, 1, secs/10,0);
  disp.setDigit(0, 0, secs%10,0);
}

void loop() 
{
  // update the encoder
  uint8_t x = encoder.read();
  if (x) 
  {
    if (x==DIR_CW) 
    {
      timer += 10;
      if (timer > 60*99) 
      { // never use more than 6 digits for minutes
        timer = 60*99;
      }
    }
    else 
    {
      timer -= 10;
      if (timer < 0) timer = 0;
    }
    printTime();
  }

  // update the button
  button.update();
  if (button.fell())
  {
    running = ! running;
    Serial.println(running?"unpaused":"paused");
    // don't stay in the alarm state forever when paused
    if (timer < 0) timer = 0;
  }

  // update the timer
  unsigned long newmillis = millis();
  if (newmillis-prevmillis > 1000)
  {
    prevmillis = newmillis;
    if (running)
    {
        switch(timer)
        {
          case -1: timer =  0; break;
          case  0: timer = -1; break;
          default: timer -= 1; break;
        }
        printTime();
        if (running && timer == 0)
        {
//          tone(9, 1000);
          Serial.println("tone");
        }
        else
        {
//          noTone(9);
          Serial.println("no tone");
        }
    }
    else
    {
      printTime();
      Serial.println("no tone");
//      noTone(8);
    }
  }

  // update buzzer
  
  // update output pin
  if (running && timer > 0)
  {
    digitalWrite(8, HIGH);
  }
  else
  {
    digitalWrite(8, LOW);
  }
 
}
